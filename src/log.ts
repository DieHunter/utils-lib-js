import { ILogOneLine, ILogLoop } from "./types"

export const logOneLine: ILogOneLine = (str: string, overwrite: boolean = false, warp: boolean = true): void => {
    if (overwrite) {
        process.stdout.clearLine(0)
        process.stdout.cursorTo(0)
    }
    process.stdout.write(str)
    warp && process.stdout.write("\n")
}
const renderLoop = (str) => logOneLine(str, true, false);
const defaultLoopList = [`\\`, `|`, `/`, `—`, `—`]// 默认加载动画
export const logLoop: ILogLoop = (opts = {}) => {
    const { loopList = defaultLoopList, isStop = false, timer = 100 } = opts;
    let { index = 0 } = opts;
    const len = loopList?.length ?? 0;
    if (!!!len) return opts;
    if (isStop) return renderLoop("\n"); // 结束循环
    if (index >= len - 1) index = 0; // 重复循环
    const str = loopList[index++];
    renderLoop(str);
    setTimeout(() => {
        opts.index = index;
        logLoop(opts);
    }, timer);
    return opts;
}
export default {
    logOneLine,
    logLoop
}