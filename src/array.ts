import { IArrayRandom, IArrayUniq, IArrayDemote, IDemoteArray, IArrayLoop } from "./types"
import { getType } from "./base"
export const arrayRandom: IArrayRandom<any[]> = arr => arr.sort(() => Math.random() - 0.5);

export const arrayUniq: IArrayUniq<any[]> = arr => Array.from(new Set(arr))

export const arrayDemote: IArrayDemote<IDemoteArray<any>> = (arr, result = []) => {
    arr.forEach(i => getType(i) === "array" ? arrayDemote(i, result) : result.push(i))
    return result;
}

export const arrayLoop: IArrayLoop = (list, current = 0) => ({ item: list[current], current: (current + 1) % list.length })
export default {
    arrayRandom,
    arrayUniq,
    arrayDemote,
    arrayLoop
}